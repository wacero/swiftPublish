.. publishSwift documentation master file, created by
   sphinx-quickstart on Sun Aug 25 22:00:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PublishSwift's documentation
========================================
These modules were created to post in a Twiter account source parameter information 
of an earthquake as soon as they are estimated by SWIFT system. 


Requirements
------------

The following packages must be installed

.. code-block:: bash
       
    # pip install tweepy
    # pip install pytz

Description
-----------

The bash script publish.sh is triggered by an email sent by USER@SWIFT_SERVER server, but its is 
possible to run publish.sh manually with the following command: 

.. code-block:: bash
    
    $ cd publishSwift
    $ cat ./test_email.txt ./runpublish.sh 
 
The script checks the email, extract source parameter information and calls control.py with a list of
parameters as in the following command to post a preliminary event

.. code-block:: bash

    $ cd publishSwift
    $ python ./control.py 21190719053934 "2119-07-19 05:39:34"  "1.4 N"  "84.8 W" 10  8.1 0.13 0.08 "./test/srcloc_mech_tsunami.jpg"

The following command post information of a manual event

.. code-block:: bash

    $ cd publishSwift
    $ python ./control.py 21190719053934 "2119-07-19 05:39:34"  "1.4 N"  "84.8 W" 10  8.1 0.13 0.08 "./test/srcloc_mech_tsunami.jpg" Manual

There is a basic test to check if the twitter publication is working correctly:

    - Rename example_twitter_account_information.json to twitter_account_information.json and fill it with twitter token information of your account. 
    - Modify configuration.py according to your working environment and the set twitter_account parameter according to your account's name as it was specified in the twitter_account_information.json file. 

Execute the test:

.. code-block:: bash
    
    $ cd publishSwift
    $ python ./test/test_twitterPublish.py

.. toctree::
    :maxdepth: 2
    :caption: Modules documentation:
    
    twitter_account_file  
    configuration    
    swift2tweet

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

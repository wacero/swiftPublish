Twitter account file
====================

***This is a json file with information about the twitter account ***

The file has the following structure

.. code-block:: bash


    {
        "ACCOUNT_NAME_A":{
                "api_key":"API_KEY",
                "api_secret":"API_SECRET",
                "access_token":"ACCES_TOKEN",
                "secret_token":"SECRET_TOKEN"
        },
        "ACCOUNT_NAME_B":{
                "api_key":"API_KEY",
                "api_secret":"API_SECRET",
                "access_token":"ACCES_TOKEN",
                "secret_token":"SECRET_TOKEN"
        }
     }

It is possible to add as much counts as you wish. The value of ACCOUNT_NAME_X must be the same set in the parameter twitter_account in configuration.py


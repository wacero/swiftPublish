Configuration file
==================

**This python configuration file sets the working directory, db and token files, residual thresholds,etc**

.. automodule:: configuration
   :members:

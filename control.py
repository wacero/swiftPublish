import configuration
import logging
import os
import sys
from swift2tweet import swiftDB
from swift2tweet import twitterPublish
from swift2tweet import utilities


def check_threshold(residualMom, residualWave):
    """
    Check if both residuals resMomMin and resWavMin are less than thresholds set in configuration.py
    """
   
    if float(residualMom) <= float(configuration.resMomMin) and float(residualWave) <= float(configuration.resWavMin):
        logging.info("Residual Moment and Residual Wave lower than thresholds: %s and %s " %(configuration.resMomMin, configuration.resWavMin))
        return 0
    else:
        logging.info("Residual Moment and Residual Wave not lower than thresholds: %s and %s " %(configuration.resMomMin, configuration.resWavMin))
        return 1
    
def retrieve_post(eventCheck):
    """
    Recover past post from a sqlite DB
    """

    select="*"
    where='eventID=%s' %eventCheck['eventID']
    try:
        eventOld=swiftDB.outputDB(select,where)
        return eventOld
    except Exception as e:
        logging.info("Fail to get event from DB : %s  " %str(e))
        #exit(1)
    
 
def saveEventDB(event):
    """
    Store in sqlite DB event dictionary and tweet_id
    """

    logging.info("Start saving of event: %s" %event['eventID'])
    swiftDB.saveDictInDB(event)

def update_event(twitter_api,event):
    """
    
    """
    
    logging.info("Start update of event %s" %event['eventID'])
    eventOld=retrieve_post(event)
    logging.info("Deleting the old tweet")
    twitterPublish.delete_post(twitter_api,eventOld[0]['tweetID'])
    tweet_id=twitterPublish.post_event(twitter_api,event)
    event['tweetID']=tweet_id
    swiftDB.updateDictInDb(event)
    
def automatic_publish(twitter_api,event):
    
           
    logging.info( "Start processing of automatic event: %s" %event )
    event['status']='Prelim.'

    if check_threshold( event['residualM'],event['residualW']) == 0:
        logging.info("Residuals are ok. Continue to check previous post")

        previous_post=retrieve_post(event)
        if previous_post:
            logging.info("Update and publish event: %s" %event['eventID'])
            update_event(twitter_api,event)
            
        else:
            logging.info ("New event, insert into DB and publish")
            tweet_id=twitterPublish.post_event(twitter_api,event)
            event['tweetID']=tweet_id
            swiftDB.saveDictInDB(event)
            logging.info( "tweet published and tweetID stored in DB" )
            
    else:
        logging.info("Residuals not in limits. Do not publish automatic results")
        exit(1)
        
def manual_publish(twitter_api,event):
    logging.info("Start to publish manual solution")
    event['status']='Rev.'
    logging.info("check if event exists")
    previous_post=retrieve_post(event)
    if previous_post:
        logging.info("Manual event exist. Update")
        update_event(twitter_api,event)
        
    else:
        logging.info("New manual event. Publish")
        tweet_id=twitterPublish.post_event(twitter_api,event)
        event['tweetID']=tweet_id
        swiftDB.saveDictInDB(event)
        logging.info( "tweet published and tweetID stored in DB" )

def check_event_age(dateString):
    '''
    Check event time against configuration.limit_hours to publish or not
    e.g. if event is older than 2 hours do not update
    '''
    date_event_local=convert_date_UTC2local(dateString)
    date_check = datetime.now() -timedelta(hours=configuration.limit_hours)
    if date_check < datetime.strptime(date_event_local,date_format):
        logging.info("Event %s too old to publish. Set limit_hours in configuration.py")
        exit(0)
        
    

def create_event_dict(lista):
    """
    Convert a python list in a python dictionary
    """
    logging.info("Parse argumentes to dicctionary")
    event={}
    event['eventID']=lista[1]
    event['time']=lista[2]
    event['latitude']=lista[3]
    event['longitude']=lista[4]
    event['depth']=lista[5]
    event['Mw']=lista[6]
    event['residualW']=lista[7]
    event['residualM']=lista[8]
    event['path2img']=lista[9].split(",")
    return event


def principal(lista):
    """
    Create DB, load twitter tokens, create twitter api object, check event antiquity and call automatic/manual publication
    
    """
    swiftDB.init_database()
    
    token=utilities.read_json_file(configuration.twitter_account_file)
    
    twitter_api=twitterPublish.connect_twitter(token[configuration.twitter_account])    

    #utilities.check_event_age(lista[2])
    
    if len(lista)==10:
        logging.info( "call automatic publish")
        event=create_event_dict(lista)
        logging.info( 'Event dictionary created')
        automatic_publish(twitter_api,event)
 
    elif len(lista)==11:
        logging.info( "call manual publish")
        event=create_event_dict(lista)
        manual_publish(twitter_api,event)

    else:
        print "wrong number of arguments. Exit"
        exit(-1)
    

arguments=sys.argv
principal(arguments)


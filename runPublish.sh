#!/bin/bash
#
# Orignially developed by W. Acero.
#
# Modified by H.Kumagai on Mar.16,2018
#    Two attached files, Web email, and Fwd email
# 
#evtinfodir=/export/home/pvjica/seis/cmt/publishSwift/
evtinfodir=$HOME/eclipse-workspace/publishSwift/
tmp=$evtinfodir/tmp.mail
log="$evtinfodir/controlSwift.log"
#email_direction=hirokumagai67@docomo.ne.jp
email_direction=acerowilson@gmail.com
cat > $tmp
idx=`grep "Subject" $tmp | awk -F'[:]' '{print $2}'`
if [[ $idx = *Fwd* ]] ; then
   swiftID=`grep "Subject" $tmp | awk -F'[:]' '{print $3}'`
   loc=`grep "Subject" $tmp | awk -F'[:]' '{print $4}'`
else
   swiftID=$idx
   loc=`grep "Subject" $tmp | awk -F'[:]' '{print $3}'`
fi
#swiftID=`grep "Subject" $tmp | awk -F'[:]' '{print $2}'`
#loc=`grep "Subject" $tmp | awk -F'[:]' '{print $3}'`

if [[ $swiftID == *CMT* ]] ; then
evtTime=`grep "Time" $tmp | head -1 | awk -F'[, ]' '{print $4 " " $5}' `
evtLat=`grep "Location" $tmp | head -1 | awk -F'[:,]' '{print $3}'`
evtLon=`grep "Location" $tmp | head -1 | awk -F'[:,]' '{print $2}'`
evtDep=`grep "Depth" $tmp | head -1 |  awk -F'[: ]' '{print $3}'`
 evtMw=`grep "^Mw" $tmp | head -1 | awk -F'[:]' '{print $2}'`
resWav=`grep "waveform" $tmp | head -1 | awk -F'[:]' '{print $2}'` 
resMom=`grep "mom_fun" $tmp | head -1 | awk -F'[:]' '{print $2}'` 
evtID=`date -d "$evtTime" +%Y%m%d%H%M%S`
evtImg=($(grep "filename"  $tmp | awk -F'[="]' '{print $3}'))
evtMan=`grep "Manual" $tmp | head -1 `

emailMW=6.0
#if [ `echo "$evtMw > $emailMW" | bc` == 1 ]; then
# cat <<EOF | mail -s $loc hirokumagai67@docomo.ne.jp &
#Date, Time: $evtTime
#Location: ${evtLon}, ${evtLat}
#Depth: $evtDep km
#Mw: $evtMw
#Residual, waveform: $resWav
#Residual, mom_fun: $resMom
#EOF
#fi

    echo "Start processing: " $evtMan $evtID "$evtTime" "$evtLat" "$evtLon"  $evtDep $evtMw $resWav $resMom "$evtinfodir/$evtID/${evtImg[0]}" >> $log
    if [ ! -d $evtinfodir/$evtID ]
        then  mkdir $evtinfodir/$evtID
    fi
    cp $tmp $evtinfodir/$evtID/$evtID.mail

    /usr/local/bin/ripmime -i $evtinfodir/$evtID/$evtID.mail -d $evtinfodir/$evtID/ --overwrite
    cd $evtinfodir
    if [ ${#evtImg[@]} == 2 ]
    then
        python ./control.py $evtID "$evtTime" "$evtLat" "$evtLon" $evtDep $evtMw $resWav $resMom "$evtinfodir/$evtID/${evtImg[0]},$evtinfodir/$evtID/${evtImg[1]}"  $evtMan
    else
        python ./control.py $evtID "$evtTime" "$evtLat" "$evtLon" $evtDep $evtMw $resWav $resMom "$evtinfodir/$evtID/${evtImg[0]}" $evtMan
    fi
else  
    echo "No earthquake" >> $log
    exit
fi

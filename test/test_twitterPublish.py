import sys
sys.path.append('../')

from swift2tweet import twitterPublish
import configuration

import json

import tweepy


print("=========Show library info==== ")
print("Library location: %s" %tweepy)
print("VERSION : %s"%tweepy.__version__  ) 
print("===============================")

path2img=('%s/test/srcloc_mech_tsunami.jpg' %configuration.workspace  , '%s/test/wvffit_v4.jpg' %configuration.workspace )

ev={'eventID':'21190719053934','time':'2119-07-19 05:39:34','status':'Prelim','latitude':1.4,\
    'longitude':-84.8, 'depth':10,'Mw':8.1, 'duration':16,'residualW':0.13,'residualM':0.08,'path2img':path2img}

tweet_post= "SWIFT ID:%s, %s, Date:%s, Lat:%s, Lon:%s, Depth: %s km, Mw: %s " \
            %(ev['eventID'],ev['status'],ev['time'],ev['latitude'],ev['longitude'],ev['depth'],ev['Mw'])



def read_json_file(json_file):
    try:
        with open(json_file) as json_data_files:
            return json.load(json_data_files)
    except Exception as e:
        print("Error in read_json_file(): %s" %(str(e)))
        raise Exception("Error in read_json_file(): %s" %(str(e)))

def test_connect_twitter():
    
    token=read_json_file(configuration.twitter_account_file)
    print("=====Test connect_twitter=====")

    try:
        twitter_api=twitterPublish.connect_twitter(token[configuration.twitter_account])
        
        print("OK--function twitterPublish.connect_twitter()" )
        print("RESULT: API object: %s" %str(dir(twitter_api)))
        print("=====End test connect_twitter=====")
   
        return twitter_api
    except Exception as e:
        
        print("ERROR in twitterPublish.connect_twitter: %s " %str(e))
        print("=====End test connect_twitter=====")

def test_post_event(): 
    print("=====Test post_event=====")
    
    try:
        twitter_api=test_connect_twitter()
        tweet_id=twitterPublish.post_event(twitter_api,ev)
        print("OK--function twitterPublish.post_event()")
        print("RESULT: Tweet id: %s" %tweet_id )
        print("=====End test post_event=====")
        return tweet_id
    except Exception as e:
        
        print("ERROR in twitterPublish.post_event(): %s " %str(e) )
        print("=====End test post_event=====")    
 
def test_delete_post():
    print("=====Test delete_post=====") 
    try:
        
        twitter_api=test_connect_twitter()
        tweet_id=test_post_event()
        twitterPublish.delete_post(twitter_api,tweet_id)
        print("OK--function twitterPublish.delete_post()")
        print("=====End delete_post=====")
    except Exception as e:
        print("ERROR in twitterPublish.delete_post(): %s " %str(e))
        print("=====End delete_post=====")    
        
test_connect_twitter()
#test_post_event()
#test_delete_post()





"""
Configure the following parameters before executing the code. 

:workspace: folder with the source code and working directory
:dbname: path to database to store event and tweet id 
:dbtable: name of the table
:resMomMin: minimal moment residual 
:resWavMin: minimal wave residual 
:twitter_account_file: path to the json file with twitter secret tokens
:twitter_account: name of the twitter account to use. Must be the same stored in twitter_account_file
:local_time_zone: time zone of the country were the seismic event
:limit_hours: Do not post information of an event after this limit
:date_format: Format to manage datetime information

"""

workspace        =   "./"
dbname           =   "%s/event.db" %workspace
dbtable          =   "events"

resMomMin=0.15
resWavMin=0.50

twitter_account_file  = '%s/%s' %(workspace,'twitter_account_information.json')
twitter_account       = 'AWACERO'
local_time_zone= 'America/Guayaquil'
limit_hours =   20000
date_format =   '%Y-%m-%d %H:%M:%S'

"""
Configure the logging format and file
"""

import os
import logging
logging.basicConfig(filename=os.path.join(workspace,"controlSwift.log"),format='%(asctime)s %(levelname)s %(message)s',level=logging.INFO)

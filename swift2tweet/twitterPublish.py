import sys
sys.path.append('../')

import configuration
import tweepy
import logging
import os

def connect_twitter(token_dict):
    """
    Connect to twitter using oauth authentication

    :param dict token_dict: dictionary with the authorization tokens as read from configuration.twitter_account_file
    :returns: tweepy api object to connect to Twitter
    :raises Exception e: General exception if there is any problem connecting to Twitter
    """ 

    try:
        oauth = tweepy.OAuthHandler(token_dict['api_key'],token_dict['api_secret'])
        oauth.set_access_token(token_dict['access_token'], token_dict['secret_token'])
        twitter_api = tweepy.API(oauth)
        return twitter_api
    except Exception as e:
        logging.info("Error in twitterPublish.connect_twitter(): %s" %str(e))
        raise Exception("Error in twitterPublish.connect_twitter(): %s" %str(e))

def post_event(twitter_api,ev):
    """
    Post estimation of source parameter calculated by SWIFT to Twitter 

    :param tweepy api twitter_api: api object to interact with twitter service
    :param dict ev: python dictionary with source parameters of an seismic event. 
    :returns: twitter_id string that identifies the tweet.  
    :raises Exception e: Genreal exception if the post fails. 
    """

    logging.info("Post event to twitter")
    tweet_post= "SWIFT ID:%s, %s, Date:%s, Lat:%s, Lon:%s, Depth: %s km, Mw: %s " \
    %(ev['eventID'],ev['status'],ev['time'],ev['latitude'],ev['longitude'],ev['depth'],ev['Mw'])

    try:
        med_ids=[ twitter_api.media_upload(i).media_id_string for i in ev['path2img'] ]
    except Exception as e:
        raise Exception("Error uploading images %s: %s" %(str(e),str(ev['path2img'])) )
    try:
        tweet_result=twitter_api.update_status(status=tweet_post,media_ids=med_ids)
        logging.info("Posted event to twitter successfully")
        return tweet_result.id
    
    except Exception as e:
        logging.info( "Error in twitterPublish.post_event(): %s" %str(e))
        raise Exception("Error in twitterPublish.post_event(): %s" %str(e))

def delete_post(twitter_api,tweet_id):
    """
    Delete a tweet posted in twitter
    
    :param tweepy API twitter_api: api object to interact with twitter service
    :param string tweet_id: string that identifies the tweet to be deleted
    :returns: tweet_id if the deletion was successful.
    :raises Exception e: Logs the exception e if the deletion fails. 
    """

    logging.info("Delete twitter: %s" %tweet_id)
    try:
        tweet_result=twitter_api.destroy_status(tweet_id)
        logging.info("Deleted tweet with id: %s" %tweet_result.id )
        return tweet_result.id 
    except Exception as e:
        logging.info("Error in twitterPublish.delete_post() %s " %str(e))

'''
Created on May 26, 2016

@author: gempa-wacero
'''


import sqlite3
import os
import configuration
import logging

   
def connectDatabase():
    dbDir = os.path.dirname(configuration.dbname)
    if not os.path.exists(dbDir):
        logging.debug("Creating DB directory")
        os.makedirs(dbDir)
    logging.debug("connecting to BD")
    con = sqlite3.connect(configuration.dbname)
    con.text_factory = bytes
    logging.debug("connection to DB established")
    return con

def closeDatabase(con):
    con.commit()
    con.close()
    logging.debug("connection to DB closed")
    
def init_database():
    logging.info("creating and starting BD")
    try: 
        if os.path.isfile(configuration.dbname):
            return 0
        con = connectDatabase()
        cur = con.cursor()
        sql = """CREATE TABLE %s (
        eventID TEXT PRIMARY KEY, time INTEGER, latitude TEXT, longitude TEXT, 
        depth INTEGER, Mw INTEGER,  residualW INTEGER, residualM INTEGER, 
         tweetID INTEGER, revision INTEGER)""" % configuration.dbtable
        logging.debug("Creating table %s" % configuration.dbtable)
        cur.execute(sql)
        closeDatabase(con)
        logging.info("Table created")
        
    except Exception as e:
        logging.info("Failed to create DB/table: %s" % str(e))
        raise Exception("Failed to create DB/table: %s" % str(e))

def saveDictInDB(eventDict):
    eventDict["table"] = configuration.dbtable
    con = connectDatabase()
    cur = con.cursor()
    sql = """INSERT INTO %s (eventID, time, latitude, longitude, depth, Mw, residualW,
    residualM, tweetID ,revision ) VALUES (:eventID, :time, :latitude, :longitude, :depth, :Mw, 
    :residualW, :residualM,:tweetID,1)""" % configuration.dbtable
    
    try:
        cur.execute(sql, eventDict)       
        logging.debug("Event '%s' added" % eventDict['eventID'])
        closeDatabase(con)
        return 0

    except sqlite3.Error, e:
        logging.debug("Failed to add event %s : %s" % (eventDict['eventID'], str(e)))
        return 1
        # return updateDictInDb(eventDict,con, cur)

def insertValue(eventDict, column, value):
    eventDict["table"] = configuration.dbtable
    con = connectDatabase()
    cur = con.cursor()
    sql = """UPDATE %s SET %s = %s WHERE eventID=%s
    """ % (configuration.dbtable, column, value, eventDict['eventID'])
    try: 
        logging.info("SQL: %s" % sql)
        cur.execute(sql, eventDict)
        logging.info("Event %s inserting")
        closeDatabase(con)
        return 0
    except sqlite3.Error, e:
        logging.error("Failed to insert value: %s, %s" % (value, str(e)))
        return 1 
    

def updateDictInDb(eventDict):
    eventDict["table"] = configuration.dbtable
    con = connectDatabase()
    cur = con.cursor()
    select = "revision"
    where = "eventID = '%s'" % (eventDict["eventID"])
    oldEventDict = outputDB(select, where)
    eventDict['revision'] = oldEventDict[0][select] + 1
    #print eventDict
    sql = """UPDATE %s SET latitude = :latitude,
    longitude = :longitude, depth = :depth, Mw = :Mw, 
     residualM = :residualM, residualW = :residualW, 
    revision = :revision, tweetID= :tweetID
    WHERE eventID = :eventID""" % configuration.dbtable
    logging.info("Trying sql %s :" % str(sql))
    try:
        cur.execute(sql, eventDict)
        logging.info("Event '%s' updated" % eventDict["eventID"])
        closeDatabase(con)
        return 0
    
    except sqlite3.Error, e:
        logging.error("Failed to update event %s : %s" % (eventDict["eventID"], str(e)))
        return 1
    closeDatabase(con)



def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d 
    

def outputDB(select="*", where=None, limit=None, desc=True):
    con = connectDatabase()
    con.row_factory = dict_factory
    con.text_factory = str
    cur = con.cursor()
    sql = "SELECT %s FROM %s " % (select, configuration.dbtable)
    if where:
        sql += "WHERE %s " % where
    sql += "ORDER BY time "
    sql += "DESC" if desc else "ASC"
    if limit:
        sql += " LIMIT %i" % limit
    # print sql
    cur.execute(sql)
    events = cur.fetchall()
    closeDatabase(con)
    return events

from datetime import datetime,timedelta
import pytz
import logging
import json
import configuration

date_format =   configuration.date_format
local_time_zone = pytz.timezone(configuration.local_time_zone)
limit_hours =   configuration.limit_hours
def convert_date_UTC2local(dateUTCstring):
    '''
    Convert a string in localtime to UTC 
    '''

    dateUTC = datetime.strptime(dateUTCstring, date_format)
    dateLocal = dateUTC.replace(tzinfo=pytz.utc).astimezone(local_time_zone)
    return dateLocal.strftime(date_format)

def check_event_age(dateString):
    '''
    Check event time against configuration.limit_hours to publish or not
    e.g. if event is older than 2 hours do not update
    '''
    date_event_local=convert_date_UTC2local(dateString)
    date_check = datetime.now() -timedelta(hours=limit_hours)
    if date_check < datetime.strptime(date_event_local,date_format):
        return 0
    else:
        return -1

    return 0

def read_json_file(json_file):
    try:
        with open(json_file) as json_data_files:
            return json.load(json_data_files)
    except Exception as e:
        logging.debug("Error in read_json_file(): %s" %(str(e)))
        raise Exception("Error in read_json_file(): %s" %(str(e)))
        
